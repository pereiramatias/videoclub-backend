from django.db import models


class NonDeleted(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_deleted=False)

class SoftDelete(models.Model):

    is_deleted = models.BooleanField(default=False)
    
    undeleted_objects = models.Manager()
    objects = NonDeleted()

    def soft_delete(self):
        self.is_deleted = True
        self.save()

    def restore(self):
        self.is_deleted = False
        self.save()

    class Meta:
        abstract = True


class Genero(SoftDelete):
    class Meta:
        verbose_name_plural = "generos"
    codigo = models.CharField(max_length=100)
    description = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.description


class Pelicula(SoftDelete):
    class Meta:
        verbose_name_plural = "peliculas"
    titulo = models.CharField(max_length=50)
    director = models.CharField(max_length=100)
    genero = models.ForeignKey(Genero, on_delete=models.CASCADE)
    estreno = models.DateField()
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titulo


class Dvd(SoftDelete):
    class Meta:
        verbose_name_plural = "dvds"
    identificador_dvd = models.CharField(max_length=100)
    pelicula = models.ForeignKey(Pelicula, on_delete=models.CASCADE)
    disponible = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.identificador_dvd + ": Pelicula: " + self.pelicula.titulo


class Cliente(SoftDelete):
    class Meta:
        verbose_name_plural = "clientes"
    dni = models.CharField(max_length=15)
    nombre = models.CharField(max_length=150)
    apellido = models.CharField(max_length=150)
    direccion = models.CharField(max_length=255)
    numero_telefono = models.CharField(max_length=100)
    correo = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.apellido + " " + self.nombre


class Alquiler(SoftDelete):
    class Meta:
        verbose_name_plural = "alquileres"
    nro_recibo = models.CharField(max_length=150)
    dni_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    fecha_alquiler = models.DateField()
    fecha_devolucion = models.DateField()
    precio = models.DecimalField(decimal_places=2, max_digits=7)
    dvd = models.ForeignKey(Dvd, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Recibo: " + str(self.nro_recibo) + " | Pelicula: " + self.dvd.pelicula.titulo + " | Cliente: " + self.dni_cliente.apellido + " " + self.dni_cliente.nombre
