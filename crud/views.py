from rest_framework.views import APIView
from rest_framework import status
from django.http import JsonResponse

from .models import Alquiler, Pelicula, Dvd, Cliente, Genero
from .serializers import AlquilerSerializer, PeliculaSerializer, DvdSerializer, ClienteSerializer, GeneroSerializer
from datetime import datetime

import json

# GET REQUEST


class AlquileresApiView(APIView):
    def get(self, request, *args, **kwargs):

        alquiler = Alquiler.objects.all()

        dni = self.request.query_params.get('dni', None)
        fecha_alquiler = self.request.query_params.get('fecha_alquiler', None)

        if dni:
            alquiler = alquiler.filter(dni_cliente__dni=dni)

        if fecha_alquiler:
            # Recibimos en el formato en Español
            fecha_formato = '%d-%m-%Y'
            try:
                fecha_solicitud = datetime.strptime(
                    fecha_alquiler, fecha_formato)
            except:
                return JsonResponse({'message': "Error", 'error': 'Formato de fecha dd-MM-YYYY'}, status=status.HTTP_403_FORBIDDEN)
            # Transformamos al formato en Ingles (común en las BD)
            fecha_alquiler = datetime.strftime(fecha_solicitud, '%Y-%m-%d')

            alquiler = alquiler.filter(fecha_alquiler=fecha_alquiler)

        serializer = AlquilerSerializer(alquiler, many=True)

        if serializer.data:
            estado = status.HTTP_200_OK
            response = {'message': 'Success', 'data': serializer.data}
        else:
            estado = status.HTTP_403_FORBIDDEN
            response = {'message': 'Error',
                        'error': "Alquileres no encontrados"}

        return JsonResponse(response, status=estado)

    def patch(self, request, id):
        try:
            alquiler = Alquiler.objects.get(id=id)
        except:
            return JsonResponse({'message': "Error", 'error': 'Alquiler no encontrado'}, status=status.HTTP_403_FORBIDDEN)

        solicitud = json.loads(request.body)

        if solicitud.get("fecha_devolucion") is not None:
            # Recibimos en el formato en Español
            fecha_formato = '%d-%m-%Y'
            try:
                fecha_solicitud = datetime.strptime(
                    solicitud['fecha_devolucion'], fecha_formato)
            except:
                return JsonResponse({'message': "Error", 'error': 'Formato de fecha dd-MM-YYYY'}, status=status.HTTP_403_FORBIDDEN)

            # Transformamos al formato en Ingles (común en las BD)
            solicitud['fecha_devolucion'] = datetime.strftime(
                fecha_solicitud, '%Y-%m-%d')

        # Modifico solo si son los campos precio y fecha_devolucion
        nueva_solicitud = {key: value for (
            key, value) in solicitud.items() if key in ['precio', 'fecha_devolucion']}

        if nueva_solicitud:
            serializer = AlquilerSerializer(
                alquiler, data=nueva_solicitud, partial=True)
            if serializer.is_valid():
                serializer.save()
                response = {'message': "Success"}
                estado = status.HTTP_200_OK
            else:
                response = {'message': "Error", "errors": "Datos no validos"}
                estado = status.HTTP_403_FORBIDDEN
        else:
            response = {'message': "Error", "errors": "Nada para actualizar"}
            estado = status.HTTP_403_FORBIDDEN

        return JsonResponse(response, status=estado)


class PeliculasApiView(APIView):
    def get(self, request, *args, **kwargs):
        pelicula = Pelicula.objects.all()

        genero = self.request.query_params.get('genero', None)

        if genero:
            pelicula = pelicula.filter(genero__description__icontains=genero)

        serializer = PeliculaSerializer(pelicula, many=True)

        if serializer.data:
            estado = status.HTTP_200_OK
            response = {'message': 'Success', 'data': serializer.data}
        else:
            estado = status.HTTP_403_FORBIDDEN
            response = {'message': 'Error', 'error': "Peliculas no econtradas"}

        return JsonResponse(response, status=estado)

    def post(self, request):

        solicitud = json.loads(request.body)

        serializer = PeliculaSerializer(data=solicitud)

        if serializer.is_valid():
            serializer.save()
            response = {'message': "Success"}
            estado = status.HTTP_200_OK
        else:
            response = {'message': "Error", "errors": serializer.errors}
            estado = status.HTTP_403_FORBIDDEN

        return JsonResponse(response, status=estado)

    def delete(self, request, id):
        try:
            pelicula = Pelicula.objects.get(id=id)
        except:
            return JsonResponse({'message': "Error", 'error': 'Pelicula no encontrada'}, status=status.HTTP_403_FORBIDDEN)

        pelicula.soft_delete()
        response = {'message': "Success"}
        estado = status.HTTP_200_OK

        return JsonResponse(response, status=estado)


class DvdsApiView(APIView):
    def get(self, request, *args, **kwargs):
        dvd = Dvd.objects.all()

        serializer = DvdSerializer(dvd, many=True)

        if serializer.data:
            estado = status.HTTP_200_OK
            response = {'message': 'Success', 'data': serializer.data}
        else:
            estado = status.HTTP_403_FORBIDDEN
            response = {'message': 'Error', 'error': 'Dvds no encontrados'}

        return JsonResponse(response, status=estado)

    def post(self, request):

        solicitud = json.loads(request.body)

        serializer = DvdSerializer(data=solicitud)
        
        try:
            check = Dvd.objects.get(identificador_dvd=solicitud['identificador_dvd'])
        except Dvd.DoesNotExist:
            check = None

        if check is not None:
            return JsonResponse({'message': "Error", 'error': 'Dvd ya existe'}, status=status.HTTP_403_FORBIDDEN)

        if serializer.is_valid():
            serializer.save()
            response = {'message': "Success"}
            estado = status.HTTP_200_OK
        else:
            response = {'message': "Error", "errors": serializer.errors}
            estado = status.HTTP_403_FORBIDDEN

        return JsonResponse(response, status=estado)


class ClientesApiView(APIView):
    def post(self, request):

        solicitud = json.loads(request.body)

        serializer = ClienteSerializer(data=solicitud)

        if serializer.is_valid():
            serializer.save()
            response = {'message': "Success"}
            estado = status.HTTP_200_OK
        else:
            response = {'message': "Error", "errors": serializer.errors}
            estado = status.HTTP_403_FORBIDDEN

        return JsonResponse(response, status=estado)

    def patch(self, request, id):
        try:
            cliente = Cliente.objects.get(id=id)
        except:
            return JsonResponse({'message': "Error", 'error': 'Cliente no encontrado'}, status=status.HTTP_403_FORBIDDEN)

        solicitud = json.loads(request.body)

        # Modifico solo si son los campos numero_telefono y correo
        nueva_solicitud = {key: value for (
            key, value) in solicitud.items() if key in ['numero_telefono', 'correo']}

        if nueva_solicitud:
            serializer = ClienteSerializer(
                cliente, data=nueva_solicitud, partial=True)
            if serializer.is_valid():
                serializer.save()
                response = {'message': "Success"}
                estado = status.HTTP_200_OK
            else:
                response = {'message': "Error", "errors": "Datos no validos"}
                estado = status.HTTP_403_FORBIDDEN
        else:
            response = {'message': "Error", "errors": "Nada para actualizar"}
            estado = status.HTTP_403_FORBIDDEN

        return JsonResponse(response, status=estado)


class GenerosApiView(APIView):
    def get(self, request, *args, **kwargs):
        genero = Genero.objects.all()

        serializer = GeneroSerializer(genero, many=True)

        if serializer.data:
            message = 'Success'
            estado = status.HTTP_200_OK
        else:
            message = 'Error'
            estado = status.HTTP_403_FORBIDDEN

        response = {'message': message, 'data': serializer.data}

        return JsonResponse(response, status=estado)

    def delete(self, request, id):
        try:
            genero = Genero.objects.get(codigo=id)
        except:
            return JsonResponse({'message': "Error", 'error': 'Genero no encontrado'}, status=status.HTTP_403_FORBIDDEN)

        genero.soft_delete()
        response = {'message': "Success"}
        estado = status.HTTP_200_OK

        return JsonResponse(response, status=estado)
