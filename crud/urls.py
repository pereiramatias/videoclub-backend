from django.urls import path
from . import views

#Armo de esta manera para diferenciar bien las funciones
urlpatterns = [
    path('alquileres/',
         views.AlquileresApiView.as_view(http_method_names=['get'])),
    path('alquileres/modificar/<int:id>/',
         views.AlquileresApiView.as_view(http_method_names=['patch'])),
    path('peliculas/',
         views.PeliculasApiView.as_view(http_method_names=['get'])),
    path('peliculas/agregar/',
         views.PeliculasApiView.as_view(http_method_names=['post'])),
    path('peliculas/eliminar/<int:id>/',
         views.PeliculasApiView.as_view(http_method_names=['delete'])),
    path('clientes/', views.ClientesApiView.as_view()),
    path('clientes/agregar/',
         views.ClientesApiView.as_view(http_method_names=['post'])),
    path('clientes/modificar/<int:id>/',
         views.ClientesApiView.as_view(http_method_names=['patch'])),
    path('dvds/', views.DvdsApiView.as_view()),
    path('dvds/agregar/',
         views.DvdsApiView.as_view(http_method_names=['post'])),
    path('generos/',
         views.GenerosApiView.as_view(http_method_names=['get'])),
    path('generos/eliminar/<int:id>/',
         views.GenerosApiView.as_view(http_method_names=['delete'])),
]
