from rest_framework.serializers import ModelSerializer
from .models import Alquiler, Cliente, Dvd, Pelicula, Genero

class GeneroSerializer(ModelSerializer):
    class Meta:
        model = Genero
        exclude = ['id', 'created_at', 'is_deleted']

class PeliculaSerializer(ModelSerializer):
    class Meta:
        model = Pelicula
        exclude = ['id', 'created_at', 'is_deleted']

    #Agregamos sin necesidad de enviar una instancia del objeto, solo el ID 
    #(lo utilizo en este caso de prueba)
    def to_representation(self, instance):
        ret = super().to_representation(instance)
        genero = GeneroSerializer(instance.genero).data
        ret['genero'] = genero['description']
        return ret


class DvdSerializer(ModelSerializer):
    class Meta:
        model = Dvd
        exclude = ['id', 'created_at', 'is_deleted']

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        pelicula = PeliculaSerializer(instance.pelicula).data
        ret['pelicula'] = pelicula['titulo']
        return ret


class ClienteSerializer(ModelSerializer):
    class Meta:
        model = Cliente
        exclude = ['id', 'created_at', 'is_deleted']


class AlquilerSerializer(ModelSerializer):
    class Meta:
        model = Alquiler
        exclude = ['id', 'created_at', 'is_deleted']

    def to_representation(self, instance):
        ret = super().to_representation(instance)
        cliente = ClienteSerializer(instance.dni_cliente).data
        ret['dni_cliente'] = cliente['dni']
        ret['dvd'] = DvdSerializer(instance.dvd).data
        return ret
