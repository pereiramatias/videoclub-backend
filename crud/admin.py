from django.contrib import admin
from .models import Genero, Pelicula, Dvd, Cliente, Alquiler

admin.register(Genero, Pelicula, Dvd, Cliente, Alquiler)(admin.ModelAdmin)
